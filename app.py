from bs4 import BeautifulSoup
import requests
import os
from jinja2 import Environment, FileSystemLoader
from services.mail import SendMail
from entities.news import NewsEntity

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
env.trim_blocks = True
env.lstrip_blocks = True
env.rstrip_blocks = True

os.environ["NLS_LANG"] = "AMERICAN_AMERICA.AL32UTF8"
korea_url = "https://www.visaforkorea-vt.com/"


class VisitKorea(object):
    def __init__(self):
        self.ping = 'pong'
        self.email_service = SendMail()

    def make_request(self):
        header = {'Accept': '*/*', 'Connection': 'keep-alive'}
        response = requests.get(korea_url, headers=header)
        html_response = response.text
        return html_response

    def get_news(self):
        soup = BeautifulSoup(self.make_request(), 'html.parser')
        list_news = soup.find_all("div", class_="ad__aligner")
        all_news = []
        for news in list_news:
            new_entity = NewsEntity()
            subject = news.find('h2', class_="ad__heading").string
            new_entity.set_subject(subject)
            contents = news.find('div', class_="ad__contents")
            content = contents.find('font')
            if content is not None:
                new_entity.set_content(content.string)
            else:
                content = ""
                list_div = contents.find_all('div')
                for div in list_div:
                    span_content = div.find('span').string
                    content = content + span_content + '<br/>'
                new_entity.set_content(content)
            all_news.append(new_entity)
        template = env.get_template('news.txt')
        html_content = template.render(all_news=all_news)
        self.email_service.send_email(html_content)


def main():
    visit_korea = VisitKorea()
    visit_korea.get_news()


if __name__ == '__main__':
    main()
