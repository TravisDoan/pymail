import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

html = """\
<html>
  <body>
    <p>Hi,<br>
       How are you?<br>
       <a href="http://www.realpython.com">Real Python</a> 
       has many great tutorials.
    </p>
  </body>
</html>
"""


class SendMail(object):
    def __init__(self):
        self.sender_email = "mailfrom"
        self.receiver_email = "mailto"
        # self.receiver_email = "minhdatplus@gmail.com"
        self.password = "mailpassword"

    def init_message(self, _html):
        message = MIMEMultipart("alternative")
        message["Subject"] = "Visit Korea News Daily"
        message["From"] = self.sender_email
        message["To"] = self.receiver_email
        message.attach(MIMEText(_html, "html"))
        return message

    def send_email(self, _html):
        context = ssl.create_default_context()
        with smtplib.SMTP("smtp.gmail.com", 587) as server:
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(self.sender_email, self.password)
            server.sendmail(
                self.sender_email, self.receiver_email, self.init_message(_html).as_string()
            )


def main():
    send_email = SendMail()
    send_email.send_email(html)


if __name__ == '__main__':
    main()
