class NewsEntity(object):
    def __init__(self):
        self.ping = 'pong'
        self.subject = None
        self.content = None

    def set_subject(self, _subject):
        self.subject = _subject

    def set_content(self, _content):
        self.content = _content

    def get_subject(self):
        return self.subject

    def get_content(self):
        return self.content
